package com.notjustmaple.fueltrak.webapi;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.*;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.*;

import android.os.AsyncTask;
import android.util.Log;


public class VehicleMakes extends AsyncTask<Integer, Void, ArrayList<String>> {

	private static final  String baseURL = "http://www.fueltrak.biz/api/vehiclelist/getmakes";
	
	protected ArrayList<String> doInBackground(Integer...params) {

		ArrayList<String> vehicleMakeList = new ArrayList<String>();
		
		HttpClient httpClient = new DefaultHttpClient();
		HttpContext localContext = new BasicHttpContext();
		String URL = baseURL + "/" + params[0].toString();
		HttpGet httpGet = new HttpGet(URL);
		
		InputStream inputStream = null;
		String result = null;
		
		try {
		       HttpResponse response = httpClient.execute(httpGet, localContext);
		       HttpEntity entity = response.getEntity();
			 	
		        
		        inputStream = entity.getContent();
		        // JSON is UTF-8 by default
		        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"), 8);
		        StringBuilder sb = new StringBuilder();

		        String line = null;
		        while ((line = reader.readLine()) != null)
		        {
		            sb.append(line + "\n");
		        }
		        result = sb.toString();
		        JSONObject obj = new JSONObject(result);
		        JSONArray itemsArray = obj.getJSONArray("Items");
		        // Add a blank line to the start of the array so that there will be
		        // a blank value when first using this data source for the spinner.
		        vehicleMakeList.add("");
		        
		        for (int i = 0; i < itemsArray.length(); i++)
		        {
		          vehicleMakeList.add( ((JSONObject)itemsArray.get(i)).getString("Value"));
		        }
		        
		} catch (Exception e) {
		   Log.e("VehicleMakes", Log.getStackTraceString(e));
		   Log.e("VehicleMakes", "Cause", e.getCause());
		}
		return (vehicleMakeList);
	}

};