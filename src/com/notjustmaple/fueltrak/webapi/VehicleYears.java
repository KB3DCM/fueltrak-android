package com.notjustmaple.fueltrak.webapi;


import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.*;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.*;

import android.os.AsyncTask;
import android.util.Log;


public class VehicleYears extends AsyncTask<Void, Void, ArrayList<String>> {

	private static final String URL = "http://www.fueltrak.biz/api/vehiclelist/getyears";
	public VehicleYearsAsyncResponse delegate = null;
	
	/**
	 * Called when the task is executed.
	 * There are no parameters for this method.
	 */
	protected ArrayList<String> doInBackground(Void...params) {

		ArrayList<String> vehicleYearList = new ArrayList<String>();
		
		HttpClient httpClient = new DefaultHttpClient();
		HttpContext localContext = new BasicHttpContext();
		HttpGet httpGet = new HttpGet(URL);
		InputStream inputStream = null;
		String result = null;
		
		try {
		       HttpResponse response = httpClient.execute(httpGet, localContext);
		       HttpEntity entity = response.getEntity();
			 	
		        
		        inputStream = entity.getContent();
		        // JSON is UTF-8 by default
		        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"), 8);
		        StringBuilder sb = new StringBuilder();

		        String line = null;
		        while ((line = reader.readLine()) != null)
		        {
		            sb.append(line + "\n");
		        }
		        result = sb.toString();
		        JSONObject obj = new JSONObject(result);
		        JSONArray itemsArray = obj.getJSONArray("Items");
		        // Add a blank entry to the start of the array so that the spinner control
		        // will show a blank value when first set.
                vehicleYearList.add("");
		        
		        for (int i = 0; i < itemsArray.length(); i++)
		        {
		          vehicleYearList.add( ((JSONObject)itemsArray.get(i)).getString("Value"));
		        }
		        
		} catch (Exception e) {
		   Log.e("getVehicleYears", Log.getStackTraceString(e));
		   Log.e("getVehicleYears", "Cause", e.getCause());
		}
		return (vehicleYearList);
	}
	
	@Override
	   protected void onPostExecute(ArrayList<String> result) {
	      delegate.processFinish(result);
	   }

};

