package com.notjustmaple.fueltrak.webapi;

import java.util.ArrayList;

/**
 * Interface used to pass the result of the Vehicle Year async task back to the fragment. 
 */
public interface VehicleYearsAsyncResponse {
	void processFinish(ArrayList<String> output);
}
