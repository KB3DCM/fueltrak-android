package com.notjustmaple.fueltrak;

import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


public class ServiceRecordEntryFragment extends Fragment {
	   @Override
	    public View onCreateView(LayoutInflater inflater, 
          ViewGroup container, Bundle savedInstanceState) {
          // Inflate the layout for this fragment
	        View view =  inflater.inflate(R.layout.fragment_service_record_entry, container, false);
	        return view;
	   }
}
