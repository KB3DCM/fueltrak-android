package com.notjustmaple.fueltrak.data;

import java.util.Date;
import java.util.Locale;
import java.text.SimpleDateFormat;
import java.text.DateFormat;
import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.notjustmaple.fueltrak.data.FillUpRecordContract;
import com.notjustmaple.fueltrak.data.FillUpRecordContract.FillUpEntry;

public class FillUpRecordDatabaseHelper extends SQLiteOpenHelper {

	// If you change the database schema, you must increment the database version.
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "FuelTrak.db";
	
	public FillUpRecordDatabaseHelper(Context context, String name,
			CursorFactory factory, int version) {
		 super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(FillUpRecordContract.SQL_CREATE_TABLE);
		
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		
	}
	
	/**
	 * Adds a new fill up entry to the database.
	 * @param user
	 * @param vehicle
	 */
	public void addFillUpRecord(String user, String vehicle, Integer odometer, float unitPrice, float fuelVolume) {
		
		try
		{
			DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss", Locale.ENGLISH);
			SQLiteDatabase db = this.getWritableDatabase();
		 
			ContentValues values = new ContentValues();
			values.put(FillUpEntry.COLUMN_NAME_USER, user); // User Name
			values.put(FillUpEntry.COLUMN_NAME_VEHICLE, vehicle); // Vehicle name
			values.put(FillUpEntry.COLUMN_NAME_ODOMETER, odometer); // Current odometer reading
			values.put(FillUpEntry.COLUMN_NAME_UNITPRICE, unitPrice); // Fuel unit price
			values.put(FillUpEntry.COLUMN_NAME_VOLUME, fuelVolume); // Amount of fuel
			values.put(FillUpEntry.COLUMN_NAME_ENTEREDON, dateFormat.format(new Date())); // Current odometer reading
         
			// Inserting Row
			db.insert(FillUpEntry.TABLE_NAME, null, values);
			db.close(); // Closing database connection
		}
		catch (android.database.SQLException e)
		{
			Log.e("FillUpRecordDatabaseHelper", e.getMessage());
		}
	}

};
