package com.notjustmaple.fueltrak.data;

import android.provider.BaseColumns;

public final class FillUpRecordContract {
	
    // To prevent someone from accidentally instantiating the contract class,
    // give it an empty constructor.
    public FillUpRecordContract() {}

    /* Inner class that defines the table contents */
    public static abstract class FillUpEntry implements BaseColumns {
        public static final String TABLE_NAME = "FillUpRecords";
        public static final String COLUMN_NAME_ENTRY_ID = "entryid";
        public static final String COLUMN_NAME_USER = "user";
        public static final String COLUMN_NAME_VEHICLE = "vehicle";
        public static final String COLUMN_NAME_ODOMETER = "odometer";
        public static final String COLUMN_NAME_UNITPRICE = "unitprice";
        public static final String COLUMN_NAME_VOLUME = "volume";
        public static final String COLUMN_NAME_ENTEREDON = "enteredon";
    };
    
	// String for creating the FillUpRecord table.
	public static final String SQL_CREATE_TABLE =
		    "CREATE TABLE IF NOT EXISTS " + FillUpEntry.TABLE_NAME + " (" +
		    		FillUpEntry._ID + " INTEGER PRIMARY KEY," +
		    		FillUpEntry.COLUMN_NAME_ENTRY_ID + "TEXT, " +
		    		FillUpEntry.COLUMN_NAME_USER + "TEXT, " +
		    		FillUpEntry.COLUMN_NAME_VEHICLE + "TEXT, " +
		    		FillUpEntry.COLUMN_NAME_ODOMETER + "TEXT, " +
		    		FillUpEntry.COLUMN_NAME_UNITPRICE + "REAL, " +
		    		FillUpEntry.COLUMN_NAME_VOLUME + "REAL, " +
		    		FillUpEntry.COLUMN_NAME_ENTEREDON + "TEXT, " +
		    " )";
	
	public static final String SQL_DROP_TABLE = 
			"DROP TABLE IF EXISTS " + FillUpEntry.TABLE_NAME;
}
