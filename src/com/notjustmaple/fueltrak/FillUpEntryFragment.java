package com.notjustmaple.fueltrak;

import android.os.Bundle;
import android.app.Fragment;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class FillUpEntryFragment extends Fragment {
	
	
	   @Override
	    public View onCreateView(LayoutInflater inflater, 
                                 ViewGroup container,
                                 Bundle savedInstanceState) {

	        // Inflate the layout for this fragment
	        View view =  inflater.inflate(R.layout.fragment_fill_up_entry, container, false);
	        return view;
	   }
	   
	    @Override
	    public void onAttach(Activity activity) {
	        super.onAttach(activity);
	    }

	    @Override
	    public void onDetach() {
	        super.onDetach();
	    }
	    
	    @Override
	    public void onStop() {
	        super.onStop();
	    }

	    @Override
	    public void onStart() {
	        super.onStart();
	    }
	    
	    @Override
	    public void onPause() {
	        super.onPause();
	    }
}
