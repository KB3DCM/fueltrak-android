package com.notjustmaple.fueltrak;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Spinner;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import com.notjustmaple.fueltrak.webapi.*;

public class UserVehicleFragment extends Fragment implements VehicleYearsAsyncResponse {
	   @Override
	    public View onCreateView(LayoutInflater inflater, 
          ViewGroup container, Bundle savedInstanceState) {
          // Inflate the layout for this fragment
	        View view =  inflater.inflate(R.layout.fragment_user_vehicle, container, false);

	        // New instance of the AsyncTask that retrieves the list of vehicle years.
	        VehicleYears yearList = new VehicleYears();
	        yearList.delegate = this;
	        // Start the background task.
	        yearList.execute();
	        
	        return view;
	   }
	   
	   /**
	    * This method is called when the vehicle year AsyncTask completes, returning the list of years.
	    */
	   public void processFinish(ArrayList<String> output){
		     populateYearsSpinner(output);
		   }
	   
	   /**
	    * Pulls the available years from the web service and populates the year spinner.
	    * @param view - View to get the spinner ID from.
	    */
	   private void populateYearsSpinner(ArrayList<String> years)
	   {
	      try
	        {
	    	  // Get the current view, which is passed in the calls to populateMakeSpinner and populateModelSpinner.
	    	  final View view = getView();
	    	  
	           Spinner yearSpinner = (Spinner)view.findViewById(R.id.spinner_Year);
	           ArrayAdapter<String> adapter = new ArrayAdapter<String>(view.getContext(), android.R.layout.simple_spinner_item, years);
	           adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	           yearSpinner.setAdapter(adapter);
	           
	           // Connect the event handler that will cause the model spinner to populate.
	           yearSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
                  public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                	  // Clear any old selections from the make and model spinners.
                	  ((Spinner)view.findViewById(R.id.spinner_Make)).setAdapter(null);
                	  ((Spinner)view.findViewById(R.id.spinner_Model)).setAdapter(null);
                	  populateMakeSpinner(view);
	              }
                  public void onNothingSelected(AdapterView<?> arg0) {
                      // do nothing
                  }
              });      
	        }
	        catch (Exception e)
	        {
	        
	        }
	   }
	   
	   /**
	    * 
	    */
	   private void populateMakeSpinner(final View view)
	   {
		   try
		   {
			  android.widget.Spinner yearSpinner = (android.widget.Spinner)getView().findViewById(R.id.spinner_Year);
			  
			  if ((String)yearSpinner.getSelectedItem() != "")
			  {
				  // Set up the parameter (year) that is sent to the make lookup.
				  Integer[] year = new Integer[1];
				  year[0] = Integer.valueOf((String)yearSpinner.getSelectedItem());
				  java.util.ArrayList<String> makes = new com.notjustmaple.fueltrak.webapi.VehicleMakes().execute(year).get();
		      
				  android.widget.Spinner makesSpinner = (android.widget.Spinner)view.findViewById(R.id.spinner_Make);
				  ArrayAdapter<String> adapter = new ArrayAdapter<String>(view.getContext(), android.R.layout.simple_spinner_item, makes);
				  adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
				  makesSpinner.setAdapter(adapter);
				  makesSpinner.invalidate();
				  
				  // Set up the selection changed handler that will populate the model spinner.
				  makesSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
	                  public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
	                	  // Clear any previous selections from the model spinner.
	                	  ((Spinner)view.findViewById(R.id.spinner_Model)).setAdapter(null);
	                	  populateModelSpinner(view);
		              }
	                  public void onNothingSelected(AdapterView<?> arg0) {
	                      // do nothing
	                  }
	              });
			  }
		   }
		   catch(InterruptedException ex)
		   {
		   }
		   catch (ExecutionException ex)
		   {
		   }
		   catch (Exception ex)
		   {
		   }
	   }
	   
	   /**
	    * 
	    */
	   private void populateModelSpinner(View view)
	   {
		   try
		   {
			  android.widget.Spinner yearSpinner = (android.widget.Spinner)getView().findViewById(R.id.spinner_Year);
			  android.widget.Spinner makeSpinner = (android.widget.Spinner)getView().findViewById(R.id.spinner_Make);
			  
			  if ( ((String)yearSpinner.getSelectedItem() != "") && ((String)makeSpinner.getSelectedItem() != "") )
			  {
				  // Set the parameters (year and make) to the model lookup. 
				  Object[] params = new Object[2];
				  params[0] = Integer.valueOf((String)yearSpinner.getSelectedItem());
				  params[1] = makeSpinner.getSelectedItem().toString();
				  
				  java.util.ArrayList<String> models = new com.notjustmaple.fueltrak.webapi.VehicleModels().execute(params).get();
		      
				  android.widget.Spinner modelSpinner = (android.widget.Spinner)view.findViewById(R.id.spinner_Model);
				  ArrayAdapter<String> adapter = new ArrayAdapter<String>(view.getContext(), android.R.layout.simple_spinner_item, models);
				  adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
				  modelSpinner.setAdapter(adapter);
			  }
		   }
		   catch(InterruptedException ex)
		   {
		   }
		   catch (ExecutionException ex)
		   {
		   }
		   catch (Exception ex)
		   {
		   }
	   }
};
