package com.notjustmaple.fueltrak;

import android.os.Bundle;
import android.widget.TextView;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class MainDashboardFragment extends Fragment {


	@Override
	    public View onCreateView(LayoutInflater inflater, 
          ViewGroup container, Bundle savedInstanceState) {
	
	        // Inflate the layout for this fragment
	        View view =  inflater.inflate(R.layout.fragment_main_dashboard, container, false);
	  
	        Bundle args = getArguments();
	        String units = args.getString("Units");
	        if (units == null) units = "Miles";
	        
	        TextView milesLogged = (TextView)view.findViewById(R.id.editTotalMilesLogged);
	        milesLogged.setText("Total " + units + "Logged: ");
	        
	        TextView averageMPG = (TextView)view.findViewById(R.id.editAverageMpg);
	        
	        if(units.equalsIgnoreCase("Miles"))
	        {
	        	averageMPG.setText("Average MPG: ");
	        }
	        else
	        {
	        	averageMPG.setText("Average KPG: ");
	        }	                
	        return view;
	   }
	
	}
